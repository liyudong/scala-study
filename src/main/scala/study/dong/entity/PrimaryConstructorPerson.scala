package study.dong.entity

// 主构造器的参数将会被编译成字段
class PrimaryConstructorPerson (val name: String, val age: Int, email:String){
  println("Just constructed another person")
  def description = s"$name is $age years old"
}
