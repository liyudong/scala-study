package study.dong.entity

class Person {

  var age = 0
}


class PrivateAgePerson {
  private var privateAge = 0

  // 如果一个类没有显示的声明一个类, 将自动创建一个无参的主构造器.
  // 这是一个辅助构造器
  def this(privateAge: Int) = {
    this() //调用主构造器
    this.privateAge = privateAge
  }

  def age = privateAge

  def age_(newValue: Int) = {
    // can't get younger (年龄不能低于默认值)
    if (newValue > privateAge) privateAge = newValue
  }
}