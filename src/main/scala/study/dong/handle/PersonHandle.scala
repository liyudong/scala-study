package study.dong.handle

import study.dong.entity.{Person, PrivateAgePerson}

object PersonHandle extends App {
  val p = new Person
  p.age = 20
  println(p.age)

  val p2= new PrivateAgePerson
  p2.age_(20)

  val p3 = new PrivateAgePerson(40)
  println(p3.age)

}


